/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;

/**
 *
 * @author madarme
 */
public class SistemaNotas {
    
    
    private Estudiante estudiantes[];

    public SistemaNotas() {
    }
    
    
    public void cargarNotas(String info)
    {
    
        String m[]=info.split("\n");
        this.estudiantes=new Estudiante[m.length];
        
        
        for(int i=0;i<m.length;i++)
        {
        String datosEstudiante[]=m[i].split(";");
        this.estudiantes[i]=new Estudiante(datosEstudiante);
        }
            
            
    
    }
    
    
    public String mayoresPromedios(){
        String msg="Los estudiantes con mayores Promedios son: ";
        float aux2=0;
       int pos = 0;
        Estudiante x;
        for (int i = 0; i < estudiantes.length; i++) {
            pos = i;
            x = estudiantes[i];
            while ((pos > 0) && (estudiantes[pos - 1].getPromedio() < x.getPromedio())) {
                estudiantes[pos] = estudiantes[pos - 1];
                pos--;
            }
            estudiantes[pos] = x;

        }
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < estudiantes.length; j++) {
               if (estudiantes[i].getPromedio()==estudiantes[j].getPromedio()) {
                msg+=estudiantes[j].getCodigo()+",";
            }
                
            }
            
            
        }
        /*for (Estudiante x : estudiantes) {
            float a[] =x.getNotas();
            float auxProm1=0;
            for (int i = 0; i < a.length; i++) 
                auxProm1+=a[i];
            aux2=auxProm1/a.length;
            if (aux2>3) {
                System.out.println(x.getCodigo()+" nota: "+aux2);
                msg+=x.getCodigo()+", ";
            }
        }*/
    
        return msg;
    }
    public String estudiantesReprobados(){
        String msg="Los estudiantes reprobados son: ";
       //float aux2=0;
        for (Estudiante x : estudiantes) {
            float a[] =x.getNotas();
            float auxProm1=0;
            for (int i = 0; i < a.length; i++) 
                auxProm1+=a[i];
           // aux2=auxProm1/a.length;
            if ((auxProm1/a.length)<3) {
               // System.out.println(x.getCodigo()+" nota: "+aux2);
                msg+=x.getCodigo()+", ";
            }
        }
    
        return msg;
    }
    
    
    public String estudiantesCantQuizAsc() {
        String msg = "";

        int pos = 0;
        int aux1;
        int aux2;
        Estudiante x;
        for (int i = 0; i < estudiantes.length; i++) {
            pos = i;
            x = estudiantes[i];
            while ((pos > 0) && (estudiantes[pos - 1].getNotas().length > x.getNotas().length)) {
                estudiantes[pos] = estudiantes[pos - 1];
                pos--;
            }
            estudiantes[pos] = x;

        }
        for (Estudiante z : estudiantes) {
            msg += z.toString();
        }
        return msg;
    }
    public String estudiantesQuizAproved(){
    String msg="";
        for (Estudiante x : estudiantes) {
            if (x.aproved()) {
                msg+=x.toString();
            }
        }
    
    return msg;
    }
    
    public String estudiantesCantQuizDesc(){
        String msg="";
        
        int pos=0;int aux1; int aux2;
        Estudiante x;
        for (int i = 0; i < estudiantes.length; i++) {
            pos=i;
            x=estudiantes[i];
            while ((pos>0)&&(estudiantes[pos-1].getNotas().length<x.getNotas().length)) {                
                estudiantes[pos]=estudiantes[pos-1];
                pos--;
            }
            estudiantes[pos]=x;
            
           
        }
     for (Estudiante z: estudiantes) {
                msg+=z.toString();
            }
        return msg;
    }
    /*
    * Se implementa un metodo el cual eliminara el quiz mas bajo obtenido por el estudiante
    * por debajo de 3.0, se muestra en pantalla codigo y notas de los estudiantes con o sin cambios.
    * el metodo solo se ejecutará una vez.
    * si desea mas ejecuciones ver la clase principal.java y habilitarla para n iteraciones.
    */
    public String removeLowQuiz(){
    String msg="";
        
        for (Estudiante x : estudiantes) {
            float a[] =x.getNotasOrdenadas(x.getNotas());
           float b[]=new float[a.length-1];
            
             if (a[a.length-1]<3) {
                    for (int i = 0; i < b.length; i++) {
                     b[i]=a[i];
                        
                 }
                  x.setNotas(b);
                    
                }
            
        }
        for (Estudiante z : estudiantes) {
            msg+=z.toString();
        }
    return msg;
    }
    public String listarEstudiantes()
    {
     String msg="";
    for(Estudiante dato:this.estudiantes)
    {
        msg+=dato.toString();
        
    }    
    
    return msg;
    }
}
